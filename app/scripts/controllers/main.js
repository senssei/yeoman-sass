'use strict';

/**
 * @ngdoc function
 * @name yeomanSassApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanSassApp
 */
angular.module('yeomanSassApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
