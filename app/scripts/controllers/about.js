'use strict';

/**
 * @ngdoc function
 * @name yeomanSassApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yeomanSassApp
 */
angular.module('yeomanSassApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
